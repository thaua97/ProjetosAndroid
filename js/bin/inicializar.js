$(document).ready(function () {
    $('.carousel').carousel();
    $('.carousel.carousel-slider').carousel({
        fullWidth: true
    });
    $('.button-collapse').sideNav();
    $('ul.tabs').tabs();
});
